require('console-stamp')(console, 'yyyy-mm-dd  HH:MM:ss.l')   ;
const ConfFromFile = require('./MonitorConfig.json')          ;
let mqtt    = require('mqtt') ;

Reset       = "\x1b[0m"
Bright      = "\x1b[1m"
Dim         = "\x1b[2m"
Underscore  = "\x1b[4m"
Blink       = "\x1b[5m"
Reverse     = "\x1b[7m"
Hidden      = "\x1b[8m"

FgBlack     = "\x1b[30m"
FgRed       = "\x1b[31m"
FgGreen     = "\x1b[32m"
FgYellow    = "\x1b[33m"
FgBlue      = "\x1b[34m"
FgMagenta   = "\x1b[35m"
FgCyan      = "\x1b[36m"
FgWhite     = "\x1b[37m"

BgBlack     = "\x1b[40m"
BgRed       = "\x1b[41m"
BgGreen     = "\x1b[42m"
BgYellow    = "\x1b[43m"
BgBlue      = "\x1b[44m"
BgMagenta   = "\x1b[45m"
BgCyan      = "\x1b[46m"
BgWhite     = "\x1b[47m"

let client  = mqtt.connect( ConfFromFile.mqttHost, ConfFromFile.options );

function formatJson(js) {
 try {
    console.dir( JSON.parse( js.toString() ) );
 }
 catch(e) {
    console.log( js.toString() );
 }
};

client.on('message',function(topic, message, packet){
   console.log( "------------------------------------" ) ; 
   console.log( FgRed    , "Topic   is :[ "+ topic   +" ]", Reset );
   console.log( FgCyan   , "Message is : ", Reset  ) ; 
   formatJson( message ) ;
   console.log( "------------------------------------" ) ; 
});

client.on("connect",function(){	
   console.log("is connection done to " + ConfFromFile.mqttHost + " ? " + client.connected );
})

client.on("error",function(error){
   console.log("Can't connect" + error);
   process.exit(1)
});

client.subscribe(ConfFromFile.topic_list,{qos:1});
